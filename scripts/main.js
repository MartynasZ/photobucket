﻿/*jslint regexp: true, nomen: true, sloppy: true */
/*global require, applicationConfig, window, applicationConfig */
require.config({
    baseUrl: '/',
    paths: {
        jquery: '/scripts/libs/jquery-1.8.3.min',
        site: '/scripts/modules/site',
        validation: '/scripts/plugins/jquery.validate',
        modal: '/scripts/plugins/jquery.modal',
		bxslider: '/scripts/plugins/jquery.bxslider.min'
    },
    shim: {
		bxslider: {
			deps: ['jquery']
		},
        site: {
            deps: ['bxslider']
        },
        validation: {
            deps: ['jquery']
        },
        modal:['jquery']
    }
});
require(['jquery', 'site', 'modal', 'validation'], function ($, site) {
    var console = window.console || { log: $.noop, error: $.noop },
        maxData = [];
    if (typeof applicationConfig != 'undefined') {
        var config = applicationConfig;
    }
    if (typeof site != 'undefined') {
        site.init();
    }
});
