﻿/*jslint regexp: true, nomen: true, sloppy: true */
/*global require, define, alert, applicationConfig, location, document, window,  setTimeout, Countable */

define(['jquery', 'modal'], function ($) {

    var module = {};

    module.photoModal = function () {
        $(document).on('click', '[data-modal]', function (e) {
            e.preventDefault();
            $(this).openModal({
                closeOnBlur: false,
                onLoad: function () {
                    toggleFav();
                    validateForms();
                },onClose: function () {
                    //Do stuff on unload
                }
            });
        });

        function toggleFav (){
            $('.btn-fav').click(function(){
              $(this).toggleClass('marked');
            });
        };

        function validateForms (){
            var form = $('.js-edit-profile-form');
            if (form.length > 0) {
                console.log('validating forms');
                form.each(function () {
                    $(this).validate({
                        errorElement: 'span',
                        errorClass: 'validation-error',
                        errorPlacement: function (error, element) {
                            $('<span class="field-validation-error"></span>').append(error).insertAfter(element);
                        },
                        success: function (label) {
                            label.parent().remove();
                        },
                        submitHandler: function(form) {
                            if ($(form).hasClass("js-edit-profile-form")) {
                                formSubmitting();
                            } else {
                                form.submit();
                            }
                        }
                    });
                });
            }
        };

        function formSubmitting (){
            var message = ' <div class="success-user-message-container"> <div class="success user-message"> <p>Your profile has been updated</p> </div> </div>';
            $(".btn-close").trigger("click");
            $(".default-messages-container").before(message);
            setTimeout(function(){
                $('.success-user-message-container').fadeOut('slow');
            }, 4000);
        }
    };

    module.tempFuctions = function(){

        //Categories list set active
        $('.js-activate-category').click(function(e){
            $(this).toggleClass('active');
            e.preventDefault();
        });

        //Categories confirm deletion
        $('.js-delete').click(function(){
            confirm("Are you sure you want to delete?");
        });

        //Favorites controls
        $('.js-fav-select-all').click(function(){
            $('.js-fav-check').prop('checked', true);
            $('.thumb-block').addClass('selected');
        });
        $('.js-fav-select-none').click(function(){
            $('.js-fav-check').prop('checked', false);
            $('.thumb-block').removeClass('selected');
        });
        $('.js-fav-check').change(function(){
            if (!$(this).is(':checked')) {
                $(this).parents('.thumb-block').removeClass('selected');
            }else{
                $(this).parents('.thumb-block').addClass('selected');
            }
        });

        $('.js-remove-fav').click(function(){
            confirm("Are you sure you want to remove from favorites?");
        });
    };

	module.bxslider = function(){
		$('.bx-slider').removeClass('nonactive');
		$('.bx-slider').bxSlider({
			mode: 'horizontal',
			nextText: '&nbsp;',
			prevText: '&nbsp;',
			speed: 500,
			pager: false,
            auto: true,
			infiniteLoop: true,
		});
	}
	
    module.init = function () {
        module.photoModal();
        module.tempFuctions();
		module.bxslider();
    };

    return module;
});